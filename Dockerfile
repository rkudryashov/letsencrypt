FROM nginx

COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY ./start_nginx.sh ./

ENTRYPOINT ["./start_nginx.sh"]
