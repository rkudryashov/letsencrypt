FROM certbot/certbot

COPY ./start_lets.sh .

ENTRYPOINT ["/bin/sh"]

CMD ["./start_lets.sh"]