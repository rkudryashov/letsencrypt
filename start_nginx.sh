#!/usr/bin/env bash

sed -i -e s/marker.domain/$DOMAIN_NAME/ /etc/nginx/conf.d/default.conf

nginx -g "daemon off;"
