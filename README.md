##requirements
Docker-CE https://docs.docker.com/install/
Docker-compose https://docs.docker.com/compose/install/

##Add domain name in ```nginx_lets.env```

##to deploy app:
```
docker-compose up -d
```

##SSL files would be created in ```./nginx/ssl/live/domain_name/```
