#!/usr/bin/env sh

certbot certonly \
    --webroot --register-unsafely-without-email \
    --agree-tos --webroot-path=/data/letsencrypt \
    -d $DOMAIN_NAME